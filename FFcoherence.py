from __future__ import division
import time
import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.signal as sig
import multiprocessing as mp
from functools import partial
import argparse
import nds2
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34, 
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'axes.prop_cycle': plt.cycler(color=plt.cm.Dark2.colors),
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.facecolor':'w',
                     'legend.fontsize': 18,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor' : 'w'})

def parseArgs():
    '''
    Get user argument inputs.  There are three necessary arguments:
    '''
    parser = argparse.ArgumentParser(description='FFcoherence computes the coherence of channels with DARM.  It takes in a channel name, start gpstime and stop gpstime, and produces a coherence plot.')
    parser.add_argument('channels', type=str, nargs='+',
                        help='String. Channel to find coherence with DARM')
    parser.add_argument('gpsStart', type=int,
                        help='Int. Gather data starting from this GPS time.')
    parser.add_argument('duration', type=int,
                        help='Int. Length in seconds of data to gather.')
    parser.add_argument('--instances', '-i', type=int, default=1,
                        help='Int. Number of data chunks to compute coherence, each separated by the duration.')
    parser.add_argument('--averages', '-a', type=int, default=100,
                        help='Int. Number of averages to use for the coherence calculation.')
    parser.add_argument('--nomulti', '-m', action='store_true',
                        help='Flag. If set, avoids creating a multiprocessing pool.')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='Flag. If set, prints verbose debug messages.')
    args = parser.parse_args()
    if args.debug:
        print 'Channels:', args.channels
        print 'GPS Start:', args.gpsStart
        print 'Duration:', args.duration
    return args

def getChannelData(args, instanceNumber):
    '''
    Retrieve channel data via nds2.
    Input: User arguments, and current instance number.
    Output: List output of the nds2 data return structure for all the desired channels
    '''
    if args.debug:
        startTime = time.time()
    hostServer = 'nds.ligo-wa.caltech.edu'
    portNumber = 31200
    #hostServer = 'h1nds1'
    #portNumber = 8088
    conn = nds2.connection(hostServer, portNumber)

    gpsStart = args.gpsStart + args.duration * instanceNumber # if this isn't the first instance, push up by duration
    gpsStop = args.gpsStart + args.duration * (instanceNumber + 1)
    darmChannel = 'H1:CAL-DELTAL_EXTERNAL_DQ'
    allChans = np.hstack((darmChannel, args.channels))
    if args.debug:
        print 'Gathering data from', allChans
    chanData = conn.fetch(gpsStart, gpsStop, allChans)
    conn.close()    
    if args.debug:
        print 'Gathering', args.duration, 'seconds of data from', len(allChans),'channels took', time.time() - startTime, 'seconds'
    
    return chanData

def computeTFs(chanData, averages):
    '''
    computeTFs(chanData, averages) computes the TF between the first channel and all the rest of the channels given.  TFs are computed by taking ASDs and dividing them.
    Input: 
    chanData is the output of nds2.connection.fetch for your desired channels.  It is assumed that the first channel is the one you want as the denominator of your TFs, with all the rest as individual numerators for your TFs.
    averages is an int representing the number of averages you want for your TFs.
    The output is a dictionary called cohData with length of chanData-1.  cohData's keys are the channel names besides the first.  The values are a tuple with
    '''
    # Sampling Frequency
    darmData = chanData[0]
    fs = darmData.sample_rate
    # check to make sure all sampling rates are the same:
    for chan in chanData:
        if np.abs(chan.sample_rate - fs) > 0.00001:
            print 'Error'
            print 'Channel', chan.name, 'has sampling rate of', chan.sample_rate, 'Hz, while', darmData.name, 'has sampling rate of', fs, 'Hz'
            sys.exit(0) 

    sampleNumber = len(darmData.data)
    timeLength = sampleNumber / fs
    fftLen = timeLength / averages
    nperseg = fftLen * fs
    binWidth = 1.0 / fftLen
    noverlap = 0

    TFData = {}
    xx = darmData.data
    ff, Pxx = sig.welch(xx, fs=fs, nperseg=nperseg, noverlap=noverlap)
    for chan in chanData:
        if chan.name == darmData.name:
            continue
        yy = chan.data
        ff, Pxy = sig.csd(xx, yy, fs=fs, nperseg=nperseg, noverlap=noverlap)
        TFxy = Pxy / Pxx # compute the TF from the PSDs
        TFData[chan.name] = (ff, TFxy, chan.gps_seconds, chanData[0].name)
    return TFData

def computeCoherence(chanData, averages):
    '''
    computeCoherence(chanData, averages)
    Input: 
    chanData is the output of nds2.connection.fetch for your desired channels.  It is assumed that the first channel is the one you want to use to find coherence with the remainder.
    averages is an int representing the number of averages you want for your coherence measurement.
    The output is a dictionary called cohData with length of chanData-1.  cohData's keys are the channel names besides the first.  The values are a tuple with
    '''
    # Sampling Frequency
    darmData = chanData[0]
    fs = darmData.sample_rate
    # check to make sure all sampling rates are the same:
    for chan in chanData:
        if np.abs(chan.sample_rate - fs) > 0.00001:
            print 'Error'
            print 'Channel', chan.name, 'has sampling rate of', chan.sample_rate, 'Hz, while', darmData.name, 'has sampling rate of', fs, 'Hz'
            sys.exit(0) 

    sampleNumber = len(darmData.data)
    timeLength = sampleNumber / fs
    fftLen = timeLength / averages
    nperseg = fftLen * fs
    binWidth = 1.0 / fftLen
    noverlap = 0

    cohData = {}
    xx = darmData.data
    for chan in chanData:
        if chan.name == darmData.name:
            continue
        yy = chan.data
        ff, Cxy = sig.coherence(xx, yy, fs=fs, nperseg=nperseg, noverlap=noverlap)
        cohData[chan.name] = (ff, Cxy, chan.gps_seconds, chanData[0].name)
    return cohData

def plotTFData(data, args, measType='TFs'):
    '''
    Plots bode plot of TF provided
    '''
    fig = plt.figure(figsize=(16,12))
    s1 = plt.subplot(211)
    s2 = plt.subplot(212)

    saveTxtHeader = 'Frequency [Hz]'
    saveData = np.array([])
    for name in data.keys():
        ff = data[name][0]
        if saveData.size == 0: # If saveData is empty
            saveData = np.copy(ff) # copy the frequency vector into it
        spec = data[name][1]
        gpsStart = data[name][2]
        firstChanName = data[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveTxtHeader = '{0}, {1} Mag, Phase [radians]'.format(saveTxtHeader, label)
        saveData = np.vstack([saveData, np.abs(spec), np.angle(spec)])

        s1.loglog(ff, np.abs(spec), label=label, rasterized=True)
        s2.semilogx(ff, 180.0/np.pi*np.angle(spec), rasterized=True)

    binWidth = np.round(ff[1] - ff[0], 3)
    s1.set_title('LSC Feedforward {0}'.format(measType).replace('_', ' '))
    s1.set_ylabel('{0} Magnitude'.format(measType))
    s1.set_xlim([min(ff), max(ff)])
    s1.grid(which='minor', linestyle='--')
    s1.legend(loc='lower left')

    s2.set_xlabel('Frequency [Hz]\nGPS Start = {0}, Duration = {1} s, Averages = {2}, Bin Width = {3} Hz'.format(gpsStart, args.duration, args.averages, binWidth))
    s2.set_ylabel('Phase [degs]'.format(measType))
    s2.set_xlim([min(ff), max(ff)])
    s2.set_ylim([-180, 180])
    s2.set_yticks(range(-180, 181, 45))
    s2.grid(which='minor', linestyle='--')

    # Save the txt 
    saveFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), measType, gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    txtSaveFilename = './data/{0}/{1}.txt'.format(time.strftime("%Y%m%d"), saveFilename) 
    np.savetxt(txtSaveFilename, saveData.T, header=saveTxtHeader)

    # Save the plot
    plotSaveFilename = './plots/{0}/{1}.pdf'.format(time.strftime("%Y%m%d"), saveFilename) 
    plt.savefig(plotSaveFilename, bbox_inches='tight')
    return plotSaveFilename

def plotCoherenceData(data, args, measType='Coherence'):
    '''
    Plots the coherence data given 
    '''
    fig = plt.figure(figsize=(16,12))
    saveTxtHeader = 'Frequency [Hz]'
    saveData = np.array([])
    for name in data.keys():
        ff = data[name][0]
        if saveData.size == 0: # If saveData is empty
            saveData = np.copy(ff) # copy the frequency vector into it
        spec = data[name][1]
        gpsStart = data[name][2]
        firstChanName = data[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveTxtHeader = '{0}, {1}'.format(saveTxtHeader, label)
        saveData = np.vstack([saveData, spec])
        plt.loglog(ff, spec, label=label, rasterized=True)

    binWidth = np.round(ff[1] - ff[0], 3)
    plt.title('LSC Feedforward {0}'.format(measType).replace('_', ' '))
    plt.xlabel('Frequency [Hz]\nGPS Start = {0}, Duration = {1} s, Averages = {2}, Bin Width = {3} Hz'.format(gpsStart, args.duration, args.averages, binWidth))
    plt.ylabel('{0} Magnitude'.format(measType))
    plt.xlim([min(ff), max(ff)])
    plt.ylim([10**-2, 1])
    plt.grid(which='minor', linestyle='--')
    plt.legend(loc='lower left')
 
    # Save the txt 
    saveFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), measType, gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    txtSaveFilename = './data/{0}/{1}.txt'.format(time.strftime("%Y%m%d"), saveFilename) 
    np.savetxt(txtSaveFilename, saveData.T, header=saveTxtHeader)

    # Save the plot
    plotSaveFilename = './plots/{0}/{1}.pdf'.format(time.strftime("%Y%m%d"), saveFilename) 
    plt.savefig(plotSaveFilename, bbox_inches='tight')
    return plotSaveFilename

def plotTFAndCohData(TFdata, cohdata, args, measType='BothTFAndCoh'):
    '''
    Plots bode plot of TF provided in TFdata, together with coherence of the data
    
    === Inputs ===
    TFdata = Dictionary. Output of computeTFs()
    cohdata = Dictionary. Output of computeCoherence()
    args = argparse data structure.  Contains all user arguments.
    measType = String.  
    
    === Output ===
    Produces and saves a matplotlib PDF plot of the TF and coherence together
    Returns a string of the plot directory and name.
    '''
    fig = plt.figure(figsize=(16,12))
    s1 = plt.subplot(311)
    s2 = plt.subplot(312)
    s3 = plt.subplot(313)

    saveTFTxtHeader = 'Frequency [Hz]'
    saveTFData = np.array([])
    for name in TFdata.keys():
        ff = TFdata[name][0]
        if saveTFData.size == 0: # If saveData is empty
            saveTFData = np.copy(ff) # copy the frequency vector into it
        spec = TFdata[name][1]
        gpsStart = TFdata[name][2]
        firstChanName = TFdata[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveTFTxtHeader = '{0}, {1} Mag, Phase [radians]'.format(saveTFTxtHeader, label)
        saveTFData = np.vstack([saveTFData, np.abs(spec), np.angle(spec)])

        s1.loglog(ff, np.abs(spec), label=label, rasterized=True)
        s2.semilogx(ff, 180.0/np.pi*np.angle(spec), rasterized=True)

    saveCohTxtHeader = 'Frequency [Hz]'
    saveCohData = np.array([])
    for name in cohdata.keys():
        ff = cohdata[name][0]
        if saveCohData.size == 0: # If saveData is empty
            saveData = np.copy(ff) # copy the frequency vector into it
        spec = cohdata[name][1]
        gpsStart = cohdata[name][2]
        firstChanName = cohdata[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveCohTxtHeader = '{0}, {1}'.format(saveCohTxtHeader, label)
        saveCohData = np.vstack([saveData, spec])
        
        s3.loglog(ff, spec, label=label, rasterized=True)

    binWidth = np.round(ff[1] - ff[0], 3)
    s1.set_title('LSC Feedforward TF and Coherence')
    s1.set_ylabel('Magnitude')
    s1.set_xlim([ff[0], ff[-1]])
    s1.grid(which='minor', linestyle='--')
    s1.legend(loc='lower left')

    s2.set_ylabel('Phase [degs]'.format(measType))
    s2.set_xlim([ff[0], ff[-1]])
    s2.set_ylim([-180, 180])
    s2.set_yticks(np.arange(-180, 181, 45))
    s2.set_yticklabels(np.arange(-180,181,45))
    s2.grid(which='minor', linestyle='--')

    s3.set_xlabel('Frequency [Hz]\nGPS Start = {0}, Duration = {1} s, Averages = {2}, Bin Width = {3} Hz'.format(gpsStart, args.duration, args.averages, binWidth))
    s3.set_ylabel('Coherence $\gamma^2$')
    s3.set_xlim([ff[0], ff[-1]])
    s3.set_ylim([10**-3, 1])
    s3.grid(which='minor', linestyle='--')

    # Save the TF txt 
    saveTFFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), 'TFs', gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    fullSaveTFFilename = './data/{0}/{1}.txt'.format(time.strftime("%Y%m%d"), saveTFFilename) 
    np.savetxt(fullSaveTFFilename, saveTFData.T, header=saveTFTxtHeader)

    # Save the Coherence txt 
    saveCohFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), 'Coh', gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    fullSaveCohFilename = './data/{0}/{1}.txt'.format(time.strftime("%Y%m%d"), saveCohFilename) 
    np.savetxt(fullSaveCohFilename, saveCohData.T, header=saveCohTxtHeader)
    
    # Save the plot
    savePlotFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), measType, gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    fullSavePlotFilename = './plots/{0}/{1}.pdf'.format(time.strftime("%Y%m%d"), savePlotFilename) 
    plt.savefig(fullSavePlotFilename, bbox_inches='tight')
    return fullSavePlotFilename


def makePDFfromPDFs(pdfs):
    '''
    make a single pdf from a list of pdfs
    Input: pdfs = a np.array containing a bunch of path/to/.pdf strings.
    Output: none in the code, but produced one big pdf containing all others.
    '''
    # Create a sensible output.pdf name for all plots
    examplePDFsplits = pdfs[0].split('/') # split the path by the slashs
    outputPDF = '' # create a output name array
    for ii, split in enumerate(examplePDFsplits): # for each split by the slash
        if ii == len(examplePDFsplits)-1: # if we have the last split
            split = 'All_{0}_Plots_{1}'.format(len(pdfs), split) # change the pdf name to include the number of plots
        else:
            split += '/'
        outputPDF += '{0}'.format(split) # append all the splits to the final plot name
    
    print 'outputPDF =', outputPDF
    # Form the ghostscript command we want to execute
    command = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile={0}'.format(outputPDF)
    for pdf in pdfs:
        command = '{0} {1}'.format(command, pdf)

    # Execute the command
    os.system(command)
    return

def instanceRunner(args, instanceNumber):
    print 'Instance {0}/{1}'.format(instanceNumber + 1, args.instances)
    if args.debug:
        print
        print "'eyy I'm workin' 'ere"
        print
    chanData = getChannelData(args, instanceNumber)
    cohdata = computeCoherence(chanData, args.averages)
    TFdata = computeTFs(chanData, args.averages)
    #cohPlotName = plotCoherenceData(cohData, args)
    #TFPlotName = plotTFData(TFData, args)
    plotName = plotTFAndCohData(TFdata, cohdata, args) # Make one big plot
    return plotName
    

if __name__ == '__main__':
    startTime = time.time()
    args = parseArgs()

    # check if plots and data directories exist
    if not os.path.isdir('./plots/{0}/'.format(time.strftime("%Y%m%d"))):
        os.makedirs('./plots/{0}/'.format(time.strftime("%Y%m%d")))
    if not os.path.isdir('./data/{0}/'.format(time.strftime("%Y%m%d"))):
        os.makedirs('./data/{0}/'.format(time.strftime("%Y%m%d")))    

    # Start looping over the number of instances, (can parallelize this in the future)
    #cohPlotNames = np.array(size=args.instances)
    #TFPlotNames = np.array(size=args.instances)


    if args.nomulti: # if we want to do this iteratively (for debugging)
        plotNames = np.array([])
        for ii in np.arange(args.instances):
            resultPlotName = instanceRunner(args, ii)
            plotNames = np.append(plotNames, resultPlotName)
    else:   # Start parallelized pool
        func = partial(instanceRunner, args) # create a partial function caller with NameSpace args always first argument to func
        pool = mp.Pool()
        plotNames = pool.map(func, range(args.instances)) # call partial function "func" with iterable range = 0, 1, 2,..., args.instances
    if args.debug:
        print
        print 'Finished creating plots'
        print
    #for ii in np.arange(args.instances):
    #    # Add plot names to same array
    #    cohPlotNames = np.append(cohPlotNames, cohPlotName)
    #    TFPlotNames = np.append(TFPlotNames, TFPlotName)
    
    # Create a single pdf from all the pdfs generated above 
    makePDFfromPDFs(plotNames)
    if args.debug:
        print
        print "Job's done in ", time.time() - startTime, "seconds"
        print
    
