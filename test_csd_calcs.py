'''
test_csd_calcs.py
Tests the scipy 1.4.1 cross-spectral density calucations.

Craig Cahillane
4/20/2020 ;)
'''

import os
import sys
import time
import copy
import numpy as np
import scipy.signal as sig
import scipy.fft as fft

from scipy.stats import expon, rayleigh, norm, laplace
from scipy.optimize import curve_fit

import matplotlib as mpl
import matplotlib.pyplot as plt

from nds2utils.make_interactive_svg import make_interactive_svg

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Functions   #####

def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def mean_squared_error(xx, yy, stats_rv, params):
    '''Calculate the mean squared error from measurement (xx, yy)
    and model (scipy.stats pdf function + fit params)
    Inputs:
    xx = x-axis of the measured histogram (use mid_bins())
    yy = y-axis of the measured histogram
    stats_rv = scipy.stats random variable class (like scipy.stats.norm)
    params = parameters of the fit to the stats_rv
    Output:
    mse = mean squared error of our fit
    '''
    error = yy - stats_rv.pdf(xx, loc=params[0], scale=params[1])
    mse = np.mean(error**2)
    return mse

def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = (scale / (kappa + 1/kappa)) * np.exp(-sign * (x - loc) * scale * kappa**sign)
    return pdf


# def main(): taken out so I can have access to these in ipython terminal
# Set up figures directory and plot array
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=1)[0]
fig_dir = f'{script_dir}/figures'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

# Set up for loop
averages_array = np.array([ 
                            100,
                            1000, 
                            10000, 
                            100000,
                            ])

for avg in averages_array:
    start_time = time.time()
    # Calculate noise plus signal
    # averages = 100

    N = (avg + 1) * 1000 / 2   # number of samples
    fs = 1e4                       # Hz, sampling frequency, samples/second
    total_time = N / fs             # seconds, total time

    nperseg = 1000                  # number of samples in a single fft segment
    noverlap = nperseg // 2         # 50% overlap

    bandwidth = fs / nperseg
    overlap = noverlap / nperseg

    averages = (total_time * bandwidth - 1)/(1 - overlap) + 1

    print()
    print(f'total samples N = {N}')
    print(f'sampling frequency = {fs} Hz')
    print()
    print(f'total_time = {total_time} seconds')
    print(f'bandwidth = {bandwidth} Hz')
    print(f'overlap = {100 * overlap} %')
    print(f'averages = {averages}')

    noise_power_density = 1e-3      # power density in V**2/Hz.  Should show up in the power spectral density.
    corr_noise_power_density = 7e-4 # correlated noise power density in V**2/Hz.  
                                    # Should be the limit the CSD hits
    total_noise_power_density = noise_power_density + corr_noise_power_density

    noise_power = noise_power_density * fs / 2              # total power in the noise spectrum in V**2.  
                                                            # Equal to the variance of the gaussian noise.  
    corr_noise_power = corr_noise_power_density * fs / 2

    times = np.arange(N) / fs

    a = np.random.normal(scale=np.sqrt(noise_power), size=times.shape) # sqrt(noise_power) = sigma on guassian noise
    b = np.random.normal(scale=np.sqrt(noise_power), size=times.shape)
    c = np.random.normal(scale=np.sqrt(corr_noise_power), size=times.shape)
    corr_noise = c

    x = a + c
    y = b + c

    # Return all FFTs directly
    ff, tt, zxxs = sig.spectral_helper(x, x, fs, nperseg=nperseg, noverlap=noverlap, mode='stft')
    ff, tt, zyys = sig.spectral_helper(y, y, fs, nperseg=nperseg, noverlap=noverlap, mode='stft')

    zxx = zxxs.mean(axis=-1)
    zyy = zyys.mean(axis=-1)

    # Calculate CSDs from FFTs
    Zxys = 2 * np.conjugate(zxxs) * zyys
    Zxy = Zxys.mean(axis=-1)

    ff_index = len(ff)//2 - 1
    Zxys_mid = Zxys[ff_index, :]

    Zxys_mid_real = np.real(Zxys_mid)
    Zxys_mid_imag = np.imag(Zxys_mid)

    Zxys_mid_real_mean = np.mean(Zxys_mid_real)
    Zxys_mid_real_median = np.median(Zxys_mid_real)
    Zxys_mid_real_rms = np.sqrt(np.mean(Zxys_mid_real**2))

    Zxys_mid_imag_mean = np.mean(Zxys_mid_imag)
    Zxys_mid_imag_median = np.median(Zxys_mid_imag)
    Zxys_mid_imag_rms = np.sqrt(np.mean(Zxys_mid_imag**2))

    # Return all PSDs directly: spectral_helper() output
    ff, tt, Sxxs = sig.spectral_helper(x, x, fs, nperseg=nperseg, noverlap=noverlap)
    Sxx = Sxxs.mean(axis=-1)
    Sxx_med = (np.median(np.real(Sxxs), axis=-1) + 1j * np.median(np.imag(Sxxs), axis=-1)) / sig.median_bias(Sxxs.shape[-1])

    # PSDs in one freq bin over time
    ff_index = len(ff)//2 - 1
    Sxxs_mid = Sxxs[ff_index, :] # get array of Sxx values at a single frequency bin for every average taken
    Sxxs_mid_mean = np.mean(Sxxs_mid)
    Sxxs_mid_median = np.median(Sxxs_mid)
    Sxxs_mid_rms = np.sqrt(np.mean(Sxxs_mid**2))

    # ASDs over time
    Axxs_mid = np.sqrt(Sxxs_mid) # ASDs
    Axxs_mid_mean = np.mean(Axxs_mid)
    Axxs_mid_median = np.median(Axxs_mid)
    Axxs_mid_rms = np.sqrt(np.mean(Axxs_mid**2))

    # Return all PSDs directly: spectral_helper() output
    ff, tt, Saas = sig.spectral_helper(a, a, fs, nperseg=nperseg, noverlap=noverlap)
    Saa = Saas.mean(axis=-1)
    Saa_med = (np.median(np.real(Saas), axis=-1) + 1j * np.median(np.imag(Saas), axis=-1)) / sig.median_bias(Saas.shape[-1])

    # PSDs in one freq bin over time
    ff_index = len(ff)//2 - 1
    Saas_mid = Saas[ff_index, :] # get array of Saa values at a single frequency bin for every average taken
    Saas_mid_mean = np.mean(Saas_mid)
    Saas_mid_median = np.median(Saas_mid)
    Saas_mid_rms = np.sqrt(np.mean(Saas_mid**2))

    # ASDs over time
    Aaas_mid = np.sqrt(Saas_mid) # ASDs
    Aaas_mid_mean = np.mean(Aaas_mid)
    Aaas_mid_median = np.median(Aaas_mid)
    Aaas_mid_rms = np.sqrt(np.mean(Aaas_mid**2))

    # Return all PSDs directly: spectral_helper() output
    ff, tt, Sccs = sig.spectral_helper(c, c, fs, nperseg=nperseg, noverlap=noverlap)
    Scc = Sccs.mean(axis=-1)
    Scc_med = (np.median(np.real(Sccs), axis=-1) + 1j * np.median(np.imag(Sccs), axis=-1)) / sig.median_bias(Sccs.shape[-1])

    # PSDs in one freq bin over time
    ff_index = len(ff)//2 - 1
    Sccs_mid = Sccs[ff_index, :] # get array of Scc values at a single frequency bin for every average taken
    Sccs_mid_mean = np.mean(Sccs_mid)
    Sccs_mid_median = np.median(Sccs_mid)
    Sccs_mid_rms = np.sqrt(np.mean(Sccs_mid**2))

    # ASDs over time
    Accs_mid = np.sqrt(Sccs_mid) # ASDs
    Accs_mid_mean = np.mean(Accs_mid)
    Accs_mid_median = np.median(Accs_mid)
    Accs_mid_rms = np.sqrt(np.mean(Accs_mid**2))

    # Return all CSDs directly: spectral_helper() CSD output
    ff, tt, Sxys = sig.spectral_helper(x, y, fs, nperseg=nperseg, noverlap=noverlap)
    Sxy = Sxys.mean(axis=-1)

    Sxys_mid = Sxys[ff_index, :]

    Sxys_mid_real = np.real(Sxys_mid)
    Sxys_mid_imag = np.imag(Sxys_mid)

    Sxys_mid_real_mean = np.mean(Sxys_mid_real)
    Sxys_mid_real_median = np.median(Sxys_mid_real)
    Sxys_mid_real_rms = np.sqrt(np.mean(Sxys_mid_real**2))

    Sxys_mid_imag_mean = np.mean(Sxys_mid_imag)
    Sxys_mid_imag_median = np.median(Sxys_mid_imag)
    Sxys_mid_imag_rms = np.sqrt(np.mean(Sxys_mid_imag**2))

    # CSD Pab <a*|b>
    ff, tt, Sabs = sig.spectral_helper(a, b, fs, nperseg=nperseg, noverlap=noverlap)
    Sabs_mid = Sabs[ff_index, :]

    Sabs_mid_real = np.real(Sabs_mid)
    Sabs_mid_imag = np.imag(Sabs_mid)

    Sabs_mid_real_mean = np.mean(Sabs_mid_real)
    Sabs_mid_real_median = np.median(Sabs_mid_real)
    Sabs_mid_real_rms = np.sqrt(np.mean(Sabs_mid_real**2))

    Sabs_mid_imag_mean = np.mean(Sabs_mid_imag)
    Sabs_mid_imag_median = np.median(Sabs_mid_imag)
    Sabs_mid_imag_rms = np.sqrt(np.mean(Sabs_mid_imag**2))

    # Mean-averaged CSD
    f, Pxy = sig.csd(x, y, fs, nperseg=nperseg, noverlap=noverlap, average='mean')  # scipy 1.4.1

    _, Cxy = sig.coherence(x, y, fs, nperseg=nperseg, noverlap=noverlap)

    _, Pxx = sig.welch(x, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pyy = sig.welch(y, fs, nperseg=nperseg, noverlap=noverlap)

    # Median-averaged CSD
    f_med, Pxy_med = sig.csd(x, y, fs, nperseg=nperseg, noverlap=noverlap, average='median')    # scipy 1.4.1

    # _, Cxy_med = sig.coherence(x, y, fs, nperseg=nperseg, noverlap=noverlap, average='median') # 'average' not a keyword for median averaging

    # _, Pxx_med = sig.welch(x, fs, nperseg=nperseg, noverlap=noverlap, average='median')
    _, Pxx_med = sig.csd(x, x, fs, nperseg=nperseg, noverlap=noverlap, average='median')
    _, Pyy_med = sig.welch(y, fs, nperseg=nperseg, noverlap=noverlap, average='median')

    Cxy_med = (np.abs(Pxy_med)/ ( np.sqrt(Pxx_med) * np.sqrt(Pyy_med) ) )**2    # boneheaded coherence calculation

    # Correlated noise only PSDs
    _, Pcc = sig.csd(x, c, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pcc_med = sig.csd(x, c, fs, nperseg=nperseg, noverlap=noverlap, average='median')

    # Uncorrelated PSDs
    _, Paa = sig.csd(a, a, fs, nperseg=nperseg, noverlap=noverlap)
    _, Paa_med = sig.csd(a, a, fs, nperseg=nperseg, noverlap=noverlap, average='median')

    _, Pbb = sig.csd(b, b, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pbb_med = sig.csd(b, b, fs, nperseg=nperseg, noverlap=noverlap, average='median')

    # Uncorrelated CSDs
    _, Pab = sig.csd(a, b, fs, nperseg=nperseg, noverlap=noverlap)
    _, Pab_med = sig.csd(a, b, fs, nperseg=nperseg, noverlap=noverlap, average='median')


    print(f'f[1] - f[0] = {f[1] - f[0]} Hz')
    print()
    print(f'Pxx = {Pxx[50]}')
    print(f'Pyy = {Pyy[50]}')
    print(f'Pxy = {Pxy[50]}')
    print(f'abs(Pxy) = {np.abs(Pxy[50])}')
    print()
    print(f'Pxx_med = {Pxx_med[50]}')
    print(f'Pyy_med = {Pyy_med[50]}')
    print(f'Pxy_med = {Pxy_med[50]}')
    print(f'abs(Pxy_med) = {np.abs(Pxy_med[50])}')
    print()

    ###########
    # Figures #
    ###########

    # CSD mag and PSD comparison
    fig, (s1) = plt.subplots(1)


    s1.semilogy(f, np.abs(Pxy), alpha=0.5, label=r'Mean-averaged CSD $P_{xy}$')
    s1.semilogy(f_med, np.abs(Pxy_med), alpha=0.5, label=r'Median-averaged CSD $P_{xy}$')

    s1.semilogy(f, np.abs(Zxy), alpha=0.5, label=r'Mean-averaged CSD $Z_{xy}$')

    s1.semilogy(f, np.abs(Pxx), alpha=0.5, label=r'Mean-averaged PSD $P_{xx}$')
    s1.semilogy(f_med, np.abs(Pxx_med), alpha=0.5, label=r'Median-averaged CSD $P_{xx}$')

    s1.semilogy(f, np.abs(Pyy), alpha=0.5, label=r'Mean-averaged PSD $P_{yy}$')
    s1.semilogy(f_med, np.abs(Pyy_med), alpha=0.5, label=r'Median-averaged PSD $P_{yy}$')

    s1.semilogy(f, np.abs(Pab), alpha=0.5, label=r'Mean-averaged CSD $P_{ab}$')
    s1.semilogy(f_med, np.abs(Pab_med), alpha=0.5, label=r'Median-averaged CSD $P_{ab}$')

    s1.semilogy(f, np.abs(Paa), alpha=0.5, label=r'Mean-averaged PSD $P_{aa}$')
    s1.semilogy(f_med, np.abs(Paa_med), alpha=0.5, label=r'Median-averaged PSD $P_{aa}$')

    s1.semilogy(f, np.abs(Pbb), alpha=0.5, label=r'Mean-averaged PSD $P_{bb}$')
    s1.semilogy(f_med, np.abs(Pbb_med), alpha=0.5, label=r'Median-averaged PSD $P_{bb}$')

    s1.semilogy(f, total_noise_power_density * np.ones_like(f), ls='--', alpha=0.5, color='black', 
                label=f'Total noise = {total_noise_power_density:.1e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
    s1.semilogy(f, noise_power_density * np.ones_like(f), ls='-', alpha=0.5, color='black', 
                label=f'Uncorrelated noise = {noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')
    s1.semilogy(f, corr_noise_power_density * np.ones_like(f), ls=':', alpha=0.5, color='black', 
                label=f'Correlated noise = {corr_noise_power_density:.0e} ' + r'$\mathrm{V}^2/\mathrm{Hz}$')

    s1.semilogy(ff, np.abs(Sxx), label='spectral helper() mean average')


    s1.set_title(f'Cross spectral density - Averages = {averages}')
    s1.set_ylabel(r'CSD Mag [$\mathrm{V}^2/\mathrm{Hz}$]')
    s1.set_xlabel('Frequency [Hz]')

    s1.set_ylim([1e-5, 3e-3])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)

    s1.legend(loc='lower left', fontsize=14)

    plot_name = f'scipy_psd_and_csds_mean_vs_median_averaging_averages_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    make_interactive_svg(fig, full_plot_name.split('.pdf')[0])
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Full CSD and coherence direct comparison
    fig, (s1, s2, s3) = plt.subplots(3, sharex=True)

    s1.semilogy(f, np.abs(Pxy), alpha=0.5, label='Mean-averaged CSD')
    s1.semilogy(f_med, np.abs(Pxy_med), alpha=0.5, label='Median-averaged CSD')
    s1.semilogy(f, np.abs(Pcc), alpha=0.5, label='Mean-averaged correlated noise PSD')
    s1.semilogy(f_med, np.abs(Pcc_med), alpha=0.5, label='Median-averaged correlated noise PSD')
    # s1.semilogy(f, np.abs(Pcd), alpha=0.5, label='Mean-averaged correlated noise PSD')
    # s1.semilogy(f_med, np.abs(Pcd_med), alpha=0.5, label='Median-averaged correlated noise PSD')

    s2.plot(f, 180/np.pi * np.angle(Pxy), alpha=0.5, label='Mean-averaged CSD')
    s2.plot(f_med, 180/np.pi * np.angle(Pxy_med), alpha=0.5, label='Median-averaged CSD')

    s3.semilogy(f, np.abs(Cxy), alpha=0.5, label='Mean-averaged coherence')
    s3.semilogy(f_med, np.abs(Cxy_med), alpha=0.5, label='Boneheaded Median-averaged coherence')


    s1.set_ylabel(r'CSD Mag [$\mathrm{V}^2/\mathrm{Hz}$]')
    s2.set_ylabel('Phase [degrees]')
    s3.set_ylabel('Power Coherence')
    s3.set_xlabel('Frequency [Hz]')

    # s1.set_ylim([1e-6, 3e1])
    s2.set_yticks([-180, -90, 0, 90, 180])
    # s3.set_ylim([0, 1])

    s1.grid()
    s1.grid(which='minor', ls='--', alpha=0.5)
    s2.grid()
    s2.grid(which='minor', ls='--', alpha=0.5)
    s3.grid()
    s3.grid(which='minor', ls='--', alpha=0.5)

    s1.legend()

    plot_name = f'scipy_csds_and_coherence_mean_vs_median_averaging_averages_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Histogram of time-domain samples
    fig, (s1) = plt.subplots(1)

    # hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
    num_bins = 100
    low_bin  = -1.5 * noise_power
    high_bin =  1.5 * noise_power
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    xx = np.linspace(low_bin, high_bin, 1001)

    norm_a_fit_params = norm.fit(a)
    print(f'norm_a_fit_params = {norm_a_fit_params}')
    norm_a_pdf_fit = norm.pdf(xx, loc=norm_a_fit_params[0], scale=norm_a_fit_params[1])

    norm_c_fit_params = norm.fit(c)
    print(f'norm_c_fit_params = {norm_c_fit_params}')
    norm_c_pdf_fit = norm.pdf(xx, loc=norm_c_fit_params[0], scale=norm_c_fit_params[1])

    norm_x_fit_params = norm.fit(x)
    print(f'norm_x_fit_params = {norm_x_fit_params}')
    norm_x_pdf_fit = norm.pdf(xx, loc=norm_x_fit_params[0], scale=norm_x_fit_params[1])

    NNa, bins, patches = s1.hist(a, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                label='a(t) samples')
    NNc, bins, patches = s1.hist(c, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                label='c(t) samples')
    NNx, bins, patches = s1.hist(x, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                label='x(t) = a(t) + c(t) samples')

    # Calculate mean squared error
    middle_bins = mid_bins(bins)
    mse_a = mean_squared_error(middle_bins, NNa, norm, norm_a_fit_params)
    mse_c = mean_squared_error(middle_bins, NNc, norm, norm_c_fit_params)
    mse_x = mean_squared_error(middle_bins, NNx, norm, norm_x_fit_params)

    s1.plot(xx, norm_a_pdf_fit, color='C0', ls='--', alpha=0.5,
            label=  f'a(t) PDF ' + \
                    r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(\frac{-a^2}{2 \times %.2e})$'%(norm_a_fit_params[1], norm_a_fit_params[1]) + \
                    f'\n' + \
                    f'MSE = {mse_a:.1e}')
    s1.plot(xx, norm_c_pdf_fit, color='C1', ls='--', alpha=0.5,
            label=  'c(t) PDF ' + \
                    r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(\frac{-c^2}{2 \times %.2e})$'%(norm_c_fit_params[1], norm_c_fit_params[1]) + \
                    f'\n' + \
                    f'MSE = {mse_c:.1e}')
    s1.plot(xx, norm_x_pdf_fit, color='C2', ls='--', alpha=0.5,
            label=  'x(t) PDF ' + \
                    r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(\frac{-x^2}{2 \times %.2e})$'%(norm_x_fit_params[1], norm_x_fit_params[1]) + \
                    f'\n' + \
                    f'MSE = {mse_x:.1e}')

    s1.set_title(f'Time domain signal distributions - N = {N} samples')
    s1.set_xlabel(r'Sample values [$\mathrm{V}$]')
    s1.set_ylabel('Normalized Occurances')

    s1.legend()
    s1.grid()

    plot_name = f'time_domain_histograms_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()
    


    # Histogram of FFTs
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    zxxs_reshape = np.reshape(zxxs, len(ff)*len(tt))

    num_bins = 100
    low_bin = -0.1
    high_bin = 0.1
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    norm_re_fit_params = norm.fit(np.real(zxxs_reshape))
    print(f'norm_re_fit_params = {norm_re_fit_params}')
    norm_re_pdf_fit = norm.pdf(bins, loc=norm_re_fit_params[0], scale=norm_re_fit_params[1])

    norm_im_fit_params = norm.fit(np.imag(zxxs_reshape))
    print(f'norm_im_fit_params = {norm_im_fit_params}')
    norm_im_pdf_fit = norm.pdf(bins, loc=norm_im_fit_params[0], scale=norm_im_fit_params[1])

    reNN, rebins, repatches = s1.hist(  np.real(zxxs_reshape), 
                                        bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(Z_{x})$'+' samples')
    # reNN, rebins, repatches = s1.hist(np.real(zyys), bins=num_bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(Z_{y})$'+' samples')

    imNN, imbins, impatches = s2.hist(  np.imag(zxxs_reshape), 
                                        bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(Z_{x})$'+' samples')
    # imNN, imbins, impatches = s2.hist(np.imag(zyys), bins=num_bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(Z_{y})$'+' samples') 

    s1.plot(bins, norm_re_pdf_fit, color='C1', ls='--', alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(\frac{-a^2}{2 \times %.2e})$'%(norm_re_fit_params[1], norm_re_fit_params[1]))
    s2.plot(bins, norm_im_pdf_fit, color='C1', ls='--', alpha=0.5,
            label='PDF ' + r'$\frac{1}{%.2e \sqrt{2 \pi}} \exp(\frac{-c^2}{2 \times %.2e})$'%(norm_im_fit_params[1], norm_im_fit_params[1]))

    s1.set_title(f'FFT histograms - {avg} averages')
    s1.set_xlabel(r'$\Re{(\mathrm{FFT})}$ values [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')
    s1.set_ylabel('Normalized Counts')

    # s2.set_title(r'$\Im{(\mathrm{CSD})}$ ' + f'{avg} averages')
    s2.set_xlabel(r'$\Im{(\mathrm{FFT})}$ values [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')
    s2.set_ylabel('Normalized Counts')

    s1.legend()
    s1.grid()

    s2.legend()
    s2.grid()

    plot_name = f'FFT_histograms_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()   


    # Histogram of PSD
    fig, (s1) = plt.subplots(1)

    # hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
    num_bins = 100
    low_bin = 0.0
    high_bin = 0.015
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    expon_fit_params = expon.fit(Sxxs_mid)
    print(f'expon_fit_params = {expon_fit_params}')
    expon_pdf_fit = expon.pdf(bins, loc=expon_fit_params[0], scale=expon_fit_params[1])

    NN, bins, patches = s1.hist(Sxxs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$S_{xx}$'+' samples')
    NN, bins, patches = s1.hist(Saas_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$S_{aa}$'+' samples')
    NN, bins, patches = s1.hist(Sccs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$S_{cc}$'+' samples')

    s1.plot(bins, expon_pdf_fit, color='C5', 
            label=r'$S_{xx}$'+' Exponential PDF ' + r'$\frac{1}{%.2e} \exp(\frac{-x}{%.2e})$'%(expon_fit_params[1], expon_fit_params[1]))

    s1.axvline(Sxxs_mid_mean, ymin=0, ymax=np.max(NN)*1.2, color='C2', label=f'mean = {Sxxs_mid_mean:.2e}')
    s1.axvline(Sxxs_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C3', label=f'median = {Sxxs_mid_median:.2e}')
    s1.axvline(Sxxs_mid_rms, ymin=0, ymax=np.max(NN)*1.2, ls=':', color='C4', label=f'rms = {Sxxs_mid_rms:.2e}')

    s1.set_title(f'PSD distribution for freq bin = {ff[ff_index]} Hz and {avg} averages')
    s1.set_xlabel(r'PSD values [$\mathrm{V}^2/\mathrm{Hz}$]')
    s1.set_ylabel('Number of Occurances')

    s1.legend()
    s1.grid()

    plot_name = f'PSD_histogram_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # Histogram of ASD
    fig, (s1) = plt.subplots(1)

    # hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
    num_bins = 100
    low_bin = 0.0
    high_bin = 0.12
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    rayleigh_fit_params = rayleigh.fit(Axxs_mid)
    print(f'rayleigh_fit_params = {rayleigh_fit_params}')
    rayleigh_pdf_fit = rayleigh.pdf(bins, *rayleigh_fit_params)

    NN, bins, patches = s1.hist(Axxs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\sqrt{S_{xx}}$'+' samples')
    NN, bins, patches = s1.hist(Aaas_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\sqrt{S_{aa}}$'+' samples')
    NN, bins, patches = s1.hist(Accs_mid, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\sqrt{S_{cc}}$'+' samples')

    s1.plot(bins, rayleigh_pdf_fit, color='C5', 
            label='Rayleigh PDF ' + r'$\frac{r}{%.2e} \exp(-\frac{r^2}{2 \times %.2e})$'%(rayleigh_fit_params[1], rayleigh_fit_params[1]))

    s1.axvline(Axxs_mid_mean, ymin=0, ymax=np.max(NN)*1.2, color='C2', label=f'mean = {Axxs_mid_mean:.2e}')
    s1.axvline(Axxs_mid_median, ymin=0, ymax=np.max(NN)*1.2, ls='--', color='C3', label=f'median = {Axxs_mid_median:.2e}')
    s1.axvline(Axxs_mid_rms, ymin=0, ymax=np.max(NN)*1.2, ls=':', color='C4', label=f'rms = {Axxs_mid_rms:.2e}')

    s1.set_title(f'ASD distribution for freq bin = {ff[ff_index]} Hz and {avg} averages')
    s1.set_xlabel(r'ASD values [$\mathrm{V}/\sqrt{\mathrm{Hz}}$]')
    s1.set_ylabel('Number of Occurances')

    s1.legend()
    s1.grid()

    plot_name = f'ASD_histogram_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()



    # CSD real and imaginary histograms
    fig, (s1, s2) = plt.subplots(2, sharex=True)

    num_bins = 300
    low_bin = -0.005
    high_bin = 0.005
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    reNNxy, rebins, repatches = s1.hist(Sxys_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(S_{xy})$'+' samples')
    # reNN, rebins, repatches = s1.hist(Zxys_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(Z_{xy})$'+' samples')
    reNNab, rebins, repatches = s1.hist(Sabs_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Re(S_{ab})$'+' samples')

    s1.axvline(Sxys_mid_real_mean,   ymin=0, ymax=np.max(reNN)*1.2,          color='C8', label=r'$\Re(S_{xy})$'+f' mean = {Sxys_mid_real_mean:.2e}')
    s1.axvline(Sxys_mid_real_median, ymin=0, ymax=np.max(reNN)*1.2, ls='--', color='C3', label=r'$\Re(S_{xy})$'+f' median = {Sxys_mid_real_median:.2e}')
    # s1.axvline(Sxys_mid_real_rms,    ymin=0, ymax=np.max(reNN)*1.2, ls=':',  color='C4', label=r'$\Re(S_{xy})$'+f' rms = {Sxys_mid_real_rms:.2e}')

    s1.axvline(Sabs_mid_real_mean,   ymin=0, ymax=np.max(reNN)*1.2,          color='C5', label=r'$\Re(S_{ab})$'+f' mean = {Sabs_mid_real_mean:.2e}')
    s1.axvline(Sabs_mid_real_median, ymin=0, ymax=np.max(reNN)*1.2, ls='--', color='C6', label=r'$\Re(S_{ab})$'+f' median = {Sabs_mid_real_median:.2e}')
    # s1.axvline(Sabs_mid_real_rms,    ymin=0, ymax=np.max(reNN)*1.2, ls=':',  color='C7', label=r'$\Re(S_{ab})$'+f' rms = {Sabs_mid_real_rms:.2e}')

    p0 = [0, 250, 1]
    middle_bins = mid_bins(bins)
    laplace_xy_real_fit_params, laplace_xy_real_fit_cov  = curve_fit(asymmetric_laplace, middle_bins, reNNxy, p0=p0)
    print(f'laplace_xy_real_fit_params = {laplace_xy_real_fit_params}')
    laplace_xy_real_pdf_fit = asymmetric_laplace(bins, *laplace_xy_real_fit_params)

    laplace_ab_real_fit_params = laplace.fit(Sabs_mid_real)
    print(f'laplace_ab_real_fit_params = {laplace_ab_real_fit_params}')
    laplace_ab_real_pdf_fit = laplace.pdf(bins, *laplace_ab_real_fit_params)

    s1.plot(bins, laplace_xy_real_pdf_fit, color='k',  lw=4, label='Asymmetric Laplace PDF ' r'$\Re(S_{xy})$')
    s1.plot(bins, laplace_ab_real_pdf_fit, color='C9', lw=4, label='Laplace PDF ' r'$\Re(S_{ab})$')

    imNN, imbins, impatches = s2.hist(Sxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(S_{xy})$'+' samples')  
    # imNN, imbins, impatches = s2.hist(Zxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(Z_{xy})$'+' samples') 
    imNN, imbins, impatches = s2.hist(Sabs_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, label=r'$\Im(S_{ab})$'+' samples')  

    s2.axvline(Sxys_mid_imag_mean,   ymin=0, ymax=np.max(imNN)*1.2,          color='C8', label=r'$\Im(S_{xy})$'+f' mean = {Sxys_mid_imag_mean:.2e}')
    s2.axvline(Sxys_mid_imag_median, ymin=0, ymax=np.max(imNN)*1.2, ls='--', color='C3', label=r'$\Im(S_{xy})$'+f' median = {Sxys_mid_imag_median:.2e}')
    # s2.axvline(Sxys_mid_imag_rms,    ymin=0, ymax=np.max(imNN)*1.2, ls=':',  color='C4', label=r'$\Im(S_{xy})$'+f' rms = {Sxys_mid_imag_rms:.2e}')

    s2.axvline(Sabs_mid_imag_mean,   ymin=0, ymax=np.max(imNN)*1.2,          color='C5', label=r'$\Im(S_{ab})$'+f' mean = {Sabs_mid_imag_mean:.2e}')
    s2.axvline(Sabs_mid_imag_median, ymin=0, ymax=np.max(imNN)*1.2, ls='--', color='C6', label=r'$\Im(S_{ab})$'+f' median = {Sabs_mid_imag_median:.2e}')
    # s2.axvline(Sabs_mid_imag_rms,    ymin=0, ymax=np.max(imNN)*1.2, ls=':',  color='C7', label=r'$\Im(S_{ab})$'+f' rms = {Sabs_mid_imag_rms:.2e}')

    laplace_xy_imag_fit_params = laplace.fit(Sxys_mid_imag)
    print(f'laplace_xy_imag_fit_params = {laplace_xy_imag_fit_params}')
    laplace_xy_imag_pdf_fit = laplace.pdf(bins, *laplace_xy_imag_fit_params)

    laplace_ab_imag_fit_params = laplace.fit(Sabs_mid_imag)
    print(f'laplace_ab_imag_fit_params = {laplace_ab_imag_fit_params}')
    laplace_ab_imag_pdf_fit = laplace.pdf(bins, *laplace_ab_imag_fit_params)

    s2.plot(bins, laplace_xy_imag_pdf_fit, color='k',  lw=4, label='Laplace PDF ' r'$\Im(S_{xy})$')
    s2.plot(bins, laplace_ab_imag_pdf_fit, color='C9', lw=4, label='Laplace PDF ' r'$\Im(S_{ab})$')

    s1.set_title(f'CSD histograms - {avg} averages')
    s1.set_xlabel(r'$\Re{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
    s1.set_ylabel('Normalized Counts')

    # s2.set_title(r'$\Im{(\mathrm{CSD})}$ ' + f'{avg} averages')
    s2.set_xlabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
    s2.set_ylabel('Normalized Counts')

    s1.legend()
    s1.grid()

    s2.legend()
    s2.grid()

    plot_name = f'CSD_histograms_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()    



    # CSD 2D histograms
    fig, ss = plt.subplots(2, 2, sharex="col", sharey="row", figsize=(9, 9),
                            gridspec_kw=dict(   height_ratios=[1, 3],
                                                width_ratios=[3, 1])
                            )
    s1 = ss[0, 0]   # top histogram
    s2 = ss[0, 1]   # delete
    s3 = ss[1, 0]   # 2d hist
    s4 = ss[1, 1]   # right histogram

    fig.delaxes(s2) # delete

    num_bins = 100
    low_bin = -0.005
    high_bin = 0.005
    bins = np.linspace(low_bin, high_bin, num_bins+1)

    reNN, rebins, repatches = s1.hist(Sxys_mid_real, bins=bins, histtype='step', lw=2, zorder=3, density=True,
                                        label='CSD samples')  
    imNN, imbins, impatches = s4.hist(Sxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True, orientation='horizontal',
                                        label='CSD samples')  
    h, xedges, yedges, image = s3.hist2d(Sxys_mid_real, Sxys_mid_imag, bins=(rebins, imbins), density=True, 
                                        cmap=mpl.cm.copper, norm=mpl.colors.LogNorm())   

    s1.axvline(Sxys_mid_real_mean, ymin=0, ymax=np.max(reNN)*1.2, color='C2', label=f'mean = {Sxys_mid_real_mean:.2e}')
    s1.axvline(Sxys_mid_real_median, ymin=0, ymax=np.max(reNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_real_median:.2e}')
    s1.axvline(Sxys_mid_real_rms, ymin=0, ymax=np.max(reNN)*1.2, ls=':', color='C4', label=f'rms = {Sxys_mid_real_rms:.2e}')

    # imNN, imbins, impatches = s2.hist(Sxys_mid_imag, bins=bins, histtype='step', lw=2, zorder=3, density=True,
    #                             label='CSD imaginary samples')   

    s4.axhline(Sxys_mid_imag_mean, xmin=0, xmax=np.max(imNN)*1.2, color='C2', label=f'mean = {Sxys_mid_imag_mean:.2e}')
    s4.axhline(Sxys_mid_imag_median, xmin=0, xmax=np.max(imNN)*1.2, ls='--', color='C3', label=f'median = {Sxys_mid_imag_median:.2e}')
    s4.axhline(Sxys_mid_imag_rms, xmin=0, xmax=np.max(imNN)*1.2, ls=':', color='C4', label=f'rms = {Sxys_mid_imag_rms:.2e}')

    s1.set_title(f'Uncorr + correlated CSD ' + r'$P_{xy}$' + ' - {avg} averages')

    s3.set_xlabel(r'$\Re{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
    s3.set_ylabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')

    # s2.set_title(r'$\Im{(\mathrm{CSD})}$ ' + f'{avg} averages')
    # s2.set_xlabel(r'$\Im{(\mathrm{CSD})}$ values [$\mathrm{V^2}/\mathrm{Hz}$]')
    # s2.set_ylabel('Number of Occurances')

    s1.legend(fontsize=12)
    s4.legend(fontsize=12)

    s1.grid()
    s3.grid()
    s4.grid()

    # s2.legend()
    # s2.grid()

    plot_name = f'CSD_2D_histogram_{int(avg)}.pdf'
    full_plot_name = '{}/{}'.format(fig_dir, plot_name)
    plot_names = np.append(plot_names, full_plot_name)
    print('Writing plot PDF to {}'.format(full_plot_name))
    plt.savefig(full_plot_name, bbox_inches='tight')
    plt.close()    

    print()
    print(f'Finished with averages = {averages} after {time.time() - start_time} seconds')
    print()
    

# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()

# if __name__ == '__main__':
#     main()