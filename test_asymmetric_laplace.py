'''
test_asymmetric_laplace.py

Code ensures the asymmetric_laplace() function does what is supposed to.

Craig Cahillane
June 1, 2020
'''
import os
import sys
import time
import copy
import numpy as np
# import scipy.signal as sig
# import scipy.fft as fft

# from scipy.stats import expon, rayleigh, norm, chi2
# from scipy.special import kn

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=1)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])

#####   Functions   #####
def asymmetric_laplace(x, loc, scale, kappa):
    '''Defines an asymmetric laplace probability distribution on x.
    Inputs:
    x       = vector of inputs to the PDF
    loc     = center peak location of the asymmetric laplacian
    scale   = scale parameter for broadening the PDF width
    kappa   = asymmetric parameter 
    Output:
    pdf     = asymmetric laplace probability distribution function on input x
    '''
    sign = np.sign(x - loc) # either +1 or -1 
    pdf = (scale / (kappa + 1/kappa)) * np.exp(-sign * (x - loc) * scale * kappa**sign)
    return pdf

#####   Figures   #####

xxx = np.linspace(-3, 3, 1001)

fig, (s1) = plt.subplots(1)

s1.plot(xxx, asymmetric_laplace(xxx, 0, 1, 1), label=r'$m = 0, \lambda = 1, \kappa = 1$')
s1.plot(xxx, asymmetric_laplace(xxx, 0, 2, 1), label=r'$m = 1, \lambda = 2, \kappa = 1$')
s1.plot(xxx, asymmetric_laplace(xxx, 0, 1, 0.5), label=r'$m = 0, \lambda = 1, \kappa = 0.5$')
s1.plot(xxx, asymmetric_laplace(xxx, 0, 1, 5), label=r'$m = 0, \lambda = 1, \kappa = 5$')

s1.set_title('Asymmetric Laplace Distribution')

s1.grid()
s1.grid(which='minor', ls='--', alpha=0.5)
s1.legend()

plot_name = f'asymmetric_laplace_test.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()