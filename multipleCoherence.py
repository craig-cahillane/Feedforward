''' 
multipleCoherence.py
This script calculates the transfer functions from a set of inputs to an output using multiple coherence functions, not ordinary coherence functions.
This makes our TF estimates robust to high or low coherence errors common to ordinary coherence measurements.
See Bendat and Piersol "Measurement and Analysis of Random Data" Section 5.7.1

For example, we often measure angular loops with driven swept sine measurements.  These loops are notoriously cross-coupled, so if we drive one loop we drive all of them.
With ordinary coherence functions, all these other loops are considered noise.  But in principle we know what the drive is, and can measure the cross-coupling between inputs.
Once we know this, we can remove this "noise" from the loop under test, and hopefully get a more accurate TF estimate.

This method requires the inputs of all relevant loops to work.  Loops not included will be considered noise.

Author: Craig Cahillane, ccahilla@caltech.edu
September 19, 2018
'''

from __future__ import division
import time
import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc
import scipy.signal as sig
import multiprocessing as mp
from functools import partial
import argparse
import nds2
mpl.rcParams.update({'text.usetex': True,
                     'text.color':'k',
                     'lines.linewidth': 4,
                     'lines.markersize': 12,
                     'font.size': 34, 
                     'font.family': 'FreeSerif',
                     'axes.grid': True,
                     'axes.facecolor' :'w',
                     'axes.labelcolor':'k',
                     'axes.titlesize': 24,
                     'axes.labelsize': 24,
                     'xtick.color':'k',
                     'xtick.labelsize': 24,
                     'ytick.color':'k',
                     'ytick.labelsize': 24,
                     'grid.color': '#555555',
                     'legend.fontsize': 18,
                     'legend.borderpad': 0.6,
                     'figure.figsize': (16, 12),
                     'figure.facecolor': 'w'})

def parseArgs():
    '''
    Get user argument inputs.  There are three necessary arguments:
    '''
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('gpsStart', type=int,
                        help='Int. Gather data starting from this GPS time.')
    parser.add_argument('duration', type=int,
                        help='Int. Length in seconds of data to gather.')
    parser.add_argument('outputChannel', type=str,
                        help='String. Output channel to find transfer functions to')
    parser.add_argument('inputChannels', type=str, nargs='+',
                        help='String. Input channels to find tranfer functions from')
    parser.add_argument('--instances', '-i', type=int, default=1,
                        help='Int. Number of data chunks to compute coherence, each separated by the duration.')
    parser.add_argument('--averages', '-a', type=int, default=100,
                        help='Int. Number of averages to use for the coherence calculation.')
    parser.add_argument('--nomulti', '-m', action='store_true',
                        help='Flag. If set, avoids creating a multiprocessing pool.')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='Flag. If set, prints verbose debug messages.')
    args = parser.parse_args()
    if args.debug:
        print 'Output Channel:', args.outputChannel
        print 'Input Channels:', args.inputChannels
        print 'GPS Start:', args.gpsStart
        print 'Duration:', args.duration
    return args

def getChannelData(args, instanceNumber):
    '''
    Retrieve channel data via nds2.
    Input: User arguments, and current instance number.
    Output: List output of the nds2 data return structure for all the desired channels
    '''
    if args.debug:
        startTime = time.time()
    hostServer = 'nds.ligo-wa.caltech.edu'
    portNumber = 31200
    #hostServer = 'h1nds1'
    #portNumber = 8088
    conn = nds2.connection(hostServer, portNumber)

    gpsStart = args.gpsStart + args.duration * instanceNumber # if this isn't the first instance, push up by duration
    gpsStop = args.gpsStart + args.duration * (instanceNumber + 1)
    allChans = np.hstack((args.inputChannels, args.outputChannel))
    if args.debug:
        print 'Gathering data from', allChans
    chanData = conn.fetch(gpsStart, gpsStop, allChans)
    conn.close()    
    if args.debug:
        print 'Gathering', args.duration, 'seconds of data from', len(allChans),'channels took', time.time() - startTime, 'seconds'
    
    return chanData

def computeTFs(chanData, averages):
    '''
    computeTFs(chanData, averages) computes the TFs between the input channels and the output channels.  TFs are computed by taking PSDs and CSDs, putting them in a matrix, and inverting it.
    Input: 
    chanData is the output of nds2.connection.fetch for your desired channels.  It is assumed that the first channel is the one you want as the denominator of your TFs, with all the rest as individual numerators for your TFs.
    averages is an int representing the number of averages you want for your TFs.
    The output is a dictionary called cohData with length of chanData-1.  cohData's keys are the channel names besides the first.  The values are a tuple with
    '''
    # Sampling Frequency
    outData = chanData[-1]
    fs = outData.sample_rate
    # check to make sure all sampling rates are the same:
    sample_rates = np.array([fs])
    for chan in chanData:
        sample_rates = np.append(sample_rates, chan.sample_rate)
    min_fs = min(sample_rates)

    for chan in chanData:
        if chan.sample_rate > min_fs:
            relativeRate = int( chan.sample_rate / fs )
            chan.data = chan.data[0::relativeRate]
            print 'Channel {0} has sampling rate of {1} Hz, while the minimum sample rate is {3} Hz'.format(chan.name, chan.sample_rate, min_fs)
            print 'Taking every {0}th sample from {1}'.format(relativeRate, chan.name)

    # Set PSD and CSD parameters
    sampleNumber = len(outData.data)
    timeLength = sampleNumber / fs
    fftLen = timeLength / averages
    nperseg = fftLen * fs
    binWidth = 1.0 / fftLen
    noverlap = 0

    # Calculate CSD and PSD estimates, and organize them into a Spectral Matrix
    startTime = time.time()
    
    qq = len(chanData)-1 
    xx = outData.data
    ff, Gxx = sig.welch(xx, fs=fs, nperseg=nperseg, noverlap=noverlap)

    outDict = {}
    outDict['ff'] = ff
    outDict['outPSD'] = Gxx
    outDict['outName'] = outData.name

    channelNames = np.array([])
    spectralMatrix = np.zeros( (qq, qq, len(ff)) )
    for ii, chan1 in enumerate(chanData):
        if chan1.name == outData.name:
            continue
        channelNames = np.append(channelNames, chan1.name) # create array of channel names in order of addition to spectral matrix
        yy = chan1.data
        ff, Gyy = sig.welch(yy, fs=fs, nperseg=nperseg, noverlap=noverlap)
        spectralMatrix[ii, ii, :] = Gyy

        for jj, chan2 in enumerate(chanData):
            if chan2.name == outData.name or jj >= ii:
                continue            
            zz = chan.data
            ff, Gyz = sig.csd(yy, zz, fs=fs, nperseg=nperseg, noverlap=noverlap) # bottom left triangle terms
            Gzy = np.conjugate(Gyz) # top right triangle terms
            spectralMatrix[ii, jj, :] = Gyz 
            spectralMatrix[jj, ii, :] = Gzy

    outDict['inputNames'] = channelNames
    outDict['spectralMatrix'] = spectralMatrix

    # Calculate CSDs Gix, between inputs and output, and put into column vector
    spectralVector = np.zeros((qq, 1, len(ff)))
    for ii, chan1 in enumerate(chanData): 
        if chan1.name == outData.name:
            continue
        yy = chan1.data
        ff, Gyx = sig.csd(yy, xx, fs=fs, nperseg=nperseg, noverlap=noverlap) 
        spectralVector[ii, 0, :] = Gyx

    outDict['spectralVector'] = spectralVector

    # Calculate the inverse spectral matrix at every frequency point fi, 
    # then do the matrix multiplication to get the TF vector
    spectralMatrixInv = np.zeros( (qq, qq, len(ff)) )
    TFVector = np.zeros((qq, 1, len(ff)))
    for kk, fi in enumerate(ff):
        tempSpecMat = spectralMatrix[:, :, kk]
        tempSpecMatInv = np.linalg.inv(tempSpecMat)
        spectralMatrixInv[:, :, kk] = tempSpecMatInv
        
        tempSpectralVector = spectralVector[:, 0, kk]
        TFVector[:, 0, kk] = tempSpecMatInv.dot(tempSpectralVector)

    outDict['spectralMatrixInv'] = spectralMatrixInv
    outDict['TFVector'] = TFVector

    print 'Spectral Matrix calculated in {0} seconds'.format(time.time() - startTime)
    return outDict

def computeMultipleCoherence(outDict):
    '''
    computeCoherence(outDict)
    Input:
    outDict, the output of computeTFs
    Output:
    A vector of q + 1 multiple coherence functions, where q is the number of inputs.  The plus one is the output, and is the last coherence function computed.
    '''
    ff = outDict['ff']
    qqPlusOne = len(outDict['inputChannels']) + 1
    augmentedSpectralMatrix = np.zeros((qqPlusOne, qqPlusOne, len(ff)))    

    # Populate the augmented spectral matrix, with the last row and column representing the output CSDs and PSDs
    augmentedSpectralMatrix[0:qqPlusOne-2, 0:qqPlusOne-2, :] = outDict['spectralMatrix']
    augmentedSpectralMatrix[qqPlusOne-1, :, :] = outDict['spectralVector']
    augmentedSpectralMatrix[:, qqPlusOne-1, :] = np.transpose( np.conjugate(outDict['spectralVector']) )
    augmentedSpectralMatrix[qqPlusOne-1, qqPlusOne-1, :] = outDict['outPSD']

    # Calculate the inverse augmented spectral matrix at every frequency point fi, 
    # then do the matrix multiplication to get the TF vector
    augmentedSpectralMatrixInv = np.zeros( (qqPlusOne, qqPlusOne, len(ff)) )
    multipleCoherenceVector = np.zeros((qqPlusOne, 1, len(ff)))
    for kk, fi in enumerate(ff):
        tempSpecMat = augmentedSpectralMatrix[:, :, kk]
        tempSpecMatInv = np.linalg.inv(tempSpecMat)
        augmentedSpectralMatrixInv[:, :, kk] = tempSpecMatInv
        
        for ii in np.arange(qqPlusOne):
            tempPSD = np.diag(outDict['spectralMatrix'][ii, ii, kk])
        

    outDict['spectralMatrixInv'] = spectralMatrixInv
    outDict['TFVector'] = TFVector

    print 'Spectral Matrix calculated in {0} seconds'.format(time.time() - startTime)


    return cohData

def plotTFData(data, args, measType='TFs'):
    '''
    Plots bode plot of TF provided
    '''
    fig = plt.figure(figsize=(16,12))
    s1 = plt.subplot(211)
    s2 = plt.subplot(212)

    saveTxtHeader = 'Frequency [Hz]'
    saveData = np.array([])
    for name in data.keys():
        ff = data[name][0]
        if saveData.size == 0: # If saveData is empty
            saveData = np.copy(ff) # copy the frequency vector into it
        spec = data[name][1]
        gpsStart = data[name][2]
        firstChanName = data[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveTxtHeader = '{0}, {1} Mag, Phase [radians]'.format(saveTxtHeader, label)
        saveData = np.vstack([saveData, np.abs(spec), np.angle(spec)])

        s1.loglog(ff, np.abs(spec), label=label, rasterized=True)
        s2.semilogx(ff, 180.0/np.pi*np.angle(spec), rasterized=True)

    binWidth = np.round(ff[1] - ff[0], 3)
    s1.set_title('LSC Feedforward {0}'.format(measType).replace('_', ' '))
    s1.set_ylabel('{0} Magnitude'.format(measType))
    s1.set_xlim([min(ff), max(ff)])
    s1.grid(which='minor', linestyle='--')
    s1.legend(loc='lower left')

    s2.set_xlabel('Frequency [Hz]\nGPS Start = {0}, Duration = {1} s, Averages = {2}, Bin Width = {3} Hz'.format(gpsStart, args.duration, args.averages, binWidth))
    s2.set_ylabel('Phase [degs]'.format(measType))
    s2.set_xlim([min(ff), max(ff)])
    s2.set_ylim([-180, 180])
    s2.set_yticks(range(-180, 181, 45))
    s2.grid(which='minor', linestyle='--')

    # Save the txt 
    saveFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), measType, gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    txtSaveFilename = './data/{0}/{1}/{2}.txt'.format(time.strftime("%Y%m%d"), args.gpsStart, saveFilename) 
    np.savetxt(txtSaveFilename, saveData.T, header=saveTxtHeader)

    # Save the plot
    plotSaveFilename = './plots/{0}/{1}/{2}.pdf'.format(time.strftime("%Y%m%d"), args.gpsStart, saveFilename) 
    plt.savefig(plotSaveFilename, bbox_inches='tight')
    return plotSaveFilename

def plotCoherenceData(data, args, measType='Coherence'):
    '''
    Plots the coherence data given 
    '''
    fig = plt.figure(figsize=(16,12))
    saveTxtHeader = 'Frequency [Hz]'
    saveData = np.array([])
    for name in data.keys():
        ff = data[name][0]
        if saveData.size == 0: # If saveData is empty
            saveData = np.copy(ff) # copy the frequency vector into it
        spec = data[name][1]
        gpsStart = data[name][2]
        firstChanName = data[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveTxtHeader = '{0}, {1}'.format(saveTxtHeader, label)
        saveData = np.vstack([saveData, spec])
        plt.loglog(ff, spec, label=label, rasterized=True)

    binWidth = np.round(ff[1] - ff[0], 3)
    plt.title('LSC Feedforward {0}'.format(measType).replace('_', ' '))
    plt.xlabel('Frequency [Hz]\nGPS Start = {0}, Duration = {1} s, Averages = {2}, Bin Width = {3} Hz'.format(gpsStart, args.duration, args.averages, binWidth))
    plt.ylabel('{0} Magnitude'.format(measType))
    plt.xlim([min(ff), max(ff)])
    plt.ylim([10**-2, 1])
    plt.grid(which='minor', linestyle='--')
    plt.legend(loc='lower left')
 
    # Save the txt 
    saveFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), measType, gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    txtSaveFilename = './data/{0}/{1}/{2}.txt'.format(time.strftime("%Y%m%d"), args.gpsStart, saveFilename) 
    np.savetxt(txtSaveFilename, saveData.T, header=saveTxtHeader)

    # Save the plot
    plotSaveFilename = './plots/{0}/{1}/{2}.pdf'.format(time.strftime("%Y%m%d"), args.gpsStart, saveFilename) 
    plt.savefig(plotSaveFilename, bbox_inches='tight')
    return plotSaveFilename

def plotTFAndCohData(TFdata, cohdata, args, measType='BothTFAndCoh'):
    '''
    Plots bode plot of TF provided in TFdata, together with coherence of the data
    
    === Inputs ===
    TFdata = Dictionary. Output of computeTFs()
    cohdata = Dictionary. Output of computeCoherence()
    args = argparse data structure.  Contains all user arguments.
    measType = String.  
    
    === Output ===
    Produces and saves a matplotlib PDF plot of the TF and coherence together
    Returns a string of the plot directory and name.
    '''
    fig = plt.figure(figsize=(16,12))
    s1 = plt.subplot(311)
    s2 = plt.subplot(312)
    s3 = plt.subplot(313)

    saveTFTxtHeader = 'Frequency [Hz]'
    saveTFData = np.array([])
    for name in TFdata.keys():
        ff = TFdata[name][0]
        if saveTFData.size == 0: # If saveData is empty
            saveTFData = np.copy(ff) # copy the frequency vector into it
        spec = TFdata[name][1]
        gpsStart = TFdata[name][2]
        firstChanName = TFdata[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveTFTxtHeader = '{0}, {1} Mag, Phase [radians]'.format(saveTFTxtHeader, label)
        saveTFData = np.vstack([saveTFData, np.abs(spec), np.angle(spec)])

        s1.loglog(ff, np.abs(spec), label=label, rasterized=True)
        s2.semilogx(ff, 180.0/np.pi*np.angle(spec), rasterized=True)

    saveCohTxtHeader = 'Frequency [Hz]'
    saveCohData = np.array([])
    for name in cohdata.keys():
        ff = cohdata[name][0]
        if saveCohData.size == 0: # If saveData is empty
            saveData = np.copy(ff) # copy the frequency vector into it
        spec = cohdata[name][1]
        gpsStart = cohdata[name][2]
        firstChanName = cohdata[name][3]
        label = '{0}/{1}'.format(name, firstChanName).replace('_', ' ')
        saveCohTxtHeader = '{0}, {1}'.format(saveCohTxtHeader, label)
        saveCohData = np.vstack([saveData, spec])
        
        s3.loglog(ff, spec, label=label, rasterized=True)

    binWidth = np.round(ff[1] - ff[0], 3)
    s1.set_title('LSC Feedforward TF and Coherence')
    s1.set_ylabel('Magnitude')
    s1.set_xlim([ff[0], ff[-1]])
    s1.grid(which='minor', linestyle='--')
    s1.legend(loc='lower left')

    s2.set_ylabel('Phase [degs]'.format(measType))
    s2.set_xlim([ff[0], ff[-1]])
    s2.set_ylim([-180, 180])
    s2.set_yticks(np.arange(-180, 181, 45))
    s2.set_yticklabels(np.arange(-180,181,45))
    s2.grid(which='minor', linestyle='--')

    s3.set_xlabel('Frequency [Hz]\nGPS Start = {0}, Duration = {1} s, Averages = {2}, Bin Width = {3} Hz'.format(gpsStart, args.duration, args.averages, binWidth))
    s3.set_ylabel('Coherence $\gamma^2$')
    s3.set_xlim([ff[0], ff[-1]])
    s3.set_ylim([10**-3, 1])
    s3.grid(which='minor', linestyle='--')

    # Save the TF txt 
    saveTFFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), 'TFs', gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    fullSaveTFFilename = './data/{0}/{1}/{2}.txt'.format(time.strftime("%Y%m%d"), args.gpsStart, saveTFFilename) 
    np.savetxt(fullSaveTFFilename, saveTFData.T, header=saveTFTxtHeader)

    # Save the Coherence txt 
    saveCohFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), 'Coh', gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    fullSaveCohFilename = './data/{0}/{1}/{2}.txt'.format(time.strftime("%Y%m%d"), args.gpsStart, saveCohFilename) 
    np.savetxt(fullSaveCohFilename, saveCohData.T, header=saveCohTxtHeader)
    
    # Save the plot
    savePlotFilename = '{0}_DARM{1}_GPSStart_{2}_Duration_{3}s_Averages_{4}_BinWidth_{5}Hz'.format(time.strftime("%Y%m%d"), measType, gpsStart, args.duration, args.averages, binWidth).replace('.','p')
    fullSavePlotFilename = './plots/{0}/{1}/{2}.pdf'.format(time.strftime("%Y%m%d"), args.gpsStart, savePlotFilename) 
    plt.savefig(fullSavePlotFilename, bbox_inches='tight')
    return fullSavePlotFilename


def makePDFfromPDFs(pdfs):
    '''
    make a single pdf from a list of pdfs
    Input: pdfs = a np.array containing a bunch of path/to/.pdf strings.
    Output: none in the code, but produced one big pdf containing all others.
    '''
    # Create a sensible output.pdf name for all plots
    examplePDFsplits = pdfs[0].split('/') # split the path by the slashs
    outputPDF = '' # create a output name array
    for ii, split in enumerate(examplePDFsplits): # for each split by the slash
        if ii == len(examplePDFsplits)-1: # if we have the last split
            split = 'All_{0}_Plots_{1}'.format(len(pdfs), split) # change the pdf name to include the number of plots
        else:
            split += '/'
        outputPDF += '{0}'.format(split) # append all the splits to the final plot name
    
    print 'outputPDF =', outputPDF
    # Form the ghostscript command we want to execute
    command = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile={0}'.format(outputPDF)
    for pdf in pdfs:
        command = '{0} {1}'.format(command, pdf)

    # Execute the command
    os.system(command)
    return

def instanceRunner(args, instanceNumber):
    print 'Instance {0}/{1}'.format(instanceNumber + 1, args.instances)
    if args.debug:
        print
        print "'eyy I'm workin' 'ere"
        print
    chanData = getChannelData(args, instanceNumber)
    cohdata = computeCoherence(chanData, args.averages)
    TFdata = computeTFs(chanData, args.averages)
    #cohPlotName = plotCoherenceData(cohData, args)
    #TFPlotName = plotTFData(TFData, args)
    plotName = plotTFAndCohData(TFdata, cohdata, args) # Make one big plot
    return plotName
    

if __name__ == '__main__':
    startTime = time.time()
    args = parseArgs()

    # check if plots and data directories exist
    if not os.path.isdir('./plots/{0}/{1}/'.format(time.strftime("%Y%m%d"), args.gpsStart)):
        os.makedirs('./plots/{0}/{1}/'.format(time.strftime("%Y%m%d"), args.gpsStart))
    if not os.path.isdir('./data/{0}/{1}/'.format(time.strftime("%Y%m%d"), args.gpsStart)):
        os.makedirs('./data/{0}/{1}/'.format(time.strftime("%Y%m%d"), args.gpsStart))    

    # Start looping over the number of instances, (can parallelize this in the future)
    #cohPlotNames = np.array(size=args.instances)
    #TFPlotNames = np.array(size=args.instances)


    if args.nomulti: # if we want to do this iteratively (for debugging)
        plotNames = np.array([])
        for ii in np.arange(args.instances):
            resultPlotName = instanceRunner(args, ii)
            plotNames = np.append(plotNames, resultPlotName)
    else:   # Start parallelized pool
        func = partial(instanceRunner, args) # create a partial function caller with NameSpace args always first argument to func
        pool = mp.Pool()
        plotNames = pool.map(func, range(args.instances)) # call partial function "func" with iterable range = 0, 1, 2,..., args.instances
    # Store the command line for future reference
    commandLine = 'python'
    for arg in sys.argv:
        commandLine += ' {0}'.format(arg)
    commandLineTxt = './data/{0}/{1}/commandLineLog.txt'.format(time.strftime("%Y%m%d"), args.gpsStart)
    with open(commandLineTxt, "w") as textFile:
        textFile.write(commandLine)

    if args.debug:
        print
        print 'Finished creating plots'
        print
    #for ii in np.arange(args.instances):
    #    # Add plot names to same array
    #    cohPlotNames = np.append(cohPlotNames, cohPlotName)
    #    TFPlotNames = np.append(TFPlotNames, TFPlotName)
    
    # Create a single pdf from all the pdfs generated above 
    makePDFfromPDFs(plotNames)
    if args.debug:
        print
        print "Job's done in ", time.time() - startTime, "seconds"
        print
    
