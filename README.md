# Feedforward
Feedforward is a technique for eliminating noise in DARM by cleverly adding signals from MICH, PRCL, and SRCL to the DARM feedback loop. Here we study the effects of feedforward over O2 for better implementation during O3.

First, I will use python to compute coherence between DARM and SRCL for several locks during O2.  Then, we will think about whether we need to improve the filtering technique currently used.

## Getting Started
No code yet, so nothing to do!  
Probably run 
```
python whatever.py
```
to produce the output.  At LHO, you may need to source my anaconda python environment:
```
source /opt/rtcds/userapps/release/cds/h1/scripts/setup_anaconda
source activate cragenv
```

## Author
Craig Cahillane

ccahilla@caltech.edu
