'''
Tests what the distribution of the product of Gaussian random variables is.

For X ~ N(0, sigma_x), Y ~ N(0, sigma_y), and Z = X * Y,
Z should follow a the zeroth-order modified Bessel function of the second kind, 
Z ~ K_0(Abs(z)/(sigma_x * sigma_y))/(pi * sigma_x * sigma_y)

If Z = X^2, then Z should follow a chi-squared distribution with degree = 1,
Z ~ chi^2 ~ 1/sqrt(2 pi sigma_x^2 z) exp(-z/(2 * sigma_x^2))

Craig Cahillane
May 30, 2020
'''

import os
import sys
import time
import copy
import numpy as np
# import scipy.signal as sig
# import scipy.fft as fft

from scipy.stats import expon, rayleigh, norm, chi2
from scipy.special import kn

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams.update({'figure.figsize':(12,9),
                     'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 18, # 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})



#####   Set up figures directory   #####
script_path = os.path.abspath(__file__)
script_dir = script_path.rsplit('/', maxsplit=1)[0]
script_name = script_path.rsplit('/')[-1].split('.py')[0]
fig_dir = f'{script_dir}/figures/{script_name}'
print()
print('Figures made by this script will be placed in:')
print(fig_dir)
if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)

plot_names = np.array([])



#####   Functions   #####
def mid_bins(bins):
    '''Returns the middle of bins given from the native mpl.hist() output'''
    diff_bins = np.diff(bins)
    middle_bins = bins[:-1] + diff_bins
    return middle_bins

def mean_squared_error(xx, yy, stats_rv, params):
    '''Calculate the mean squared error from measurement (xx, yy)
    and model (scipy.stats pdf function + fit params)
    Inputs:
    xx = x-axis of the measured histogram (use mid_bins())
    yy = y-axis of the measured histogram
    stats_rv = scipy.stats random variable class (like scipy.stats.norm)
    params = parameters of the fit to the stats_rv
    Output:
    mse = mean squared error of our fit
    '''
    error = yy - stats_rv.pdf(xx, loc=params[0], scale=params[1])
    mse = np.mean(error**2)
    return mse


#####   Samples   #####

sample_number = 1000000

# mean_x = 0
# mean_y = 0
sigma_x = 4.0
sigma_y = 6.0
power_x = sigma_x**2
power_y = sigma_y**2

xx = np.random.normal(scale=sigma_x, size=sample_number)
yy = np.random.normal(scale=sigma_y, size=sample_number)

pp = xx**2
rr = yy**2
qq = xx * yy

#####   Figures   #####

# Histogram of time-domain samples
fig, (s1) = plt.subplots(1)

# hist, bin_edges = np.histogram(Sxxs_mid, bins=20)
num_bins = 5000
low_bin  = -4 * sigma_y
high_bin =  4 * sigma_y
bins = np.linspace(low_bin, high_bin, num_bins+1)

norm_x_fit_params = norm.fit(xx)
print(f'norm_x_fit_params = {norm_x_fit_params}')
norm_x_pdf_fit = norm.pdf(bins, loc=norm_x_fit_params[0], scale=norm_x_fit_params[1])

norm_y_fit_params = norm.fit(yy)
print(f'norm_y_fit_params = {norm_y_fit_params}')
norm_y_pdf_fit = norm.pdf(bins, loc=norm_y_fit_params[0], scale=norm_y_fit_params[1])


NNx, bins, patches = s1.hist(xx, bins=bins, histtype='step', lw=2, density=True, label=r'$X$ samples')
NNy, bins, patches = s1.hist(yy, bins=bins, histtype='step', lw=2, density=True, label=r'$Y$ samples')

# Calculate mean squared error
middle_bins = mid_bins(bins)
mse_x = mean_squared_error(middle_bins, NNx, norm, norm_x_fit_params)
mse_y = mean_squared_error(middle_bins, NNy, norm, norm_y_fit_params)

s1.plot(bins, norm_x_pdf_fit, ls='--',
        label=  f'x(t) PDF ' + \
                r'$\frac{1}{\sqrt{2 \pi %.2f}} \exp(\frac{-a^2}{2 \times %.2f})$'%(norm_x_fit_params[1]**2, norm_x_fit_params[1]**2) + \
                f'\n' + \
                f'MSE = {mse_x:.1e}')
s1.plot(bins, norm_y_pdf_fit, ls='--',
        label=  'y(t) PDF ' + \
                r'$\frac{1}{\sqrt{2 \pi %.2f}} \exp(\frac{-c^2}{2 \times %.2f})$'%(norm_y_fit_params[1]**2, norm_y_fit_params[1]**2) + \
                f'\n' + \
                f'MSE = {mse_y:.1e}')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values [$\mathrm{V}$]')
s1.set_ylabel('Normalized Occurances')

s1.legend()
s1.grid()

plot_name = f'raw_samples_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of products of random samples
fig, (s1) = plt.subplots(1)

num_bins = 5000
low_bin  = 0
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)

chi2_p_fit_params = chi2.fit(pp, fdf=1)
print(f'chi2_p_fit_params = {chi2_p_fit_params}')
chi2_p_pdf_fit = chi2.pdf(bins, 1, loc=chi2_p_fit_params[-2], scale=chi2_p_fit_params[-1])

chi2_r_fit_params = chi2.fit(rr, fdf=1)
print(f'chi2_r_fit_params = {chi2_r_fit_params}')
chi2_r_pdf_fit = chi2.pdf(bins, 1, loc=chi2_r_fit_params[-2], scale=chi2_r_fit_params[-1])
# chi2_r_pdf_fit2 = chi2.pdf(bins, *chi2_r_fit_params)

NNp, bins, patches = s1.hist(pp, bins=bins, histtype='step', lw=2, density=True, label=r'$P = X^2$ samples')
NNr, bins, patches = s1.hist(rr, bins=bins, histtype='step', lw=2, density=True, label=r'$R = Y^2$ samples')

# chi_squared_pp = 1/np.sqrt(2 * np.pi * power_x * bins) * np.exp(-bins/(2 * power_x))
# chi_squared_rr = 1/np.sqrt(2 * np.pi * power_y * bins) * np.exp(-bins/(2 * power_y))

# s1.plot(bins, chi_squared_pp, ls='--', alpha=0.5, label=r'$\chi_1^2(p)$')
# s1.plot(bins, chi_squared_rr, ls='--', alpha=0.5, label=r'$\chi_1^2(r)$')

# Calculate mean squared error
# middle_bins = mid_bins(bins)
# mse_x = mean_squared_error(middle_bins, NNx, norm, norm_x_fit_params)
# mse_y = mean_squared_error(middle_bins, NNy, norm, norm_y_fit_params)

s1.plot(bins, chi2_p_pdf_fit, ls='--', label=r'$\chi_1^2(p)$ PDF')
s1.plot(bins, chi2_r_pdf_fit, ls='--', label=r'$\chi_1^2(r)$ PDF')
# s1.plot(bins, chi2_r_pdf_fit2, ls=':', label=r'$\chi_1^2(r)$ PDF 2')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values [$\mathrm{V}^2$]')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([0, 100])
s1.set_ylim([0, 0.2])

s1.legend()
s1.grid()

plot_name = f'gaussian_product_X_squared_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# Histogram of products of random samples
fig, (s1) = plt.subplots(1)

num_bins = 5000
low_bin  = -300
high_bin =  300
bins = np.linspace(low_bin, high_bin, num_bins+1)

indicies = np.argwhere(qq > 0)
chi2_q_fit_params = chi2.fit(qq[indicies], fdf=1)
print(f'chi2_q_fit_params = {chi2_q_fit_params}')
chi2_q_pdf_fit = chi2.pdf(bins, *chi2_q_fit_params)

# norm_x_fit_params = norm.fit(xx)
# print(f'norm_x_fit_params = {norm_x_fit_params}')
# norm_x_pdf_fit = norm.pdf(bins, loc=norm_x_fit_params[0], scale=norm_x_fit_params[1])

# norm_y_fit_params = norm.fit(yy)
# print(f'norm_y_fit_params = {norm_y_fit_params}')
# norm_y_pdf_fit = norm.pdf(bins, loc=norm_y_fit_params[0], scale=norm_y_fit_params[1])

NNx, bins, patches = s1.hist(qq, bins=bins, histtype='step', lw=2, density=True, label=r'$Q = XY$ samples')

# Calculate mean squared error
# middle_bins = mid_bins(bins)
# mse_x = mean_squared_error(middle_bins, NNx, norm, norm_x_fit_params)
# mse_y = mean_squared_error(middle_bins, NNy, norm, norm_y_fit_params)

s1.plot(bins, chi2_q_pdf_fit, ls='--', label=r'$\chi_1^2(q)$ PDF')

k0 = kn(0, np.abs(bins)/(sigma_x * sigma_y)) / (np.pi * sigma_x * sigma_y)
s1.plot(bins, k0, ls=':', 
        label=r'$K_0(\frac{\|x\|}{\sigma_x \sigma_y})/\pi \sigma_x^2 \sigma_y^2$')

# s1.set_title(f'Time domain signal distributions - N = {sample_number} samples')
s1.set_xlabel(r'Sample values [$\mathrm{V}^2$]')
s1.set_ylabel('Normalized Occurances')

s1.set_xlim([-100, 100])
s1.set_ylim([0, 0.2])

s1.legend()
s1.grid()

plot_name = f'gaussian_product_XY_histograms_{sample_number}.pdf'
full_plot_name = '{}/{}'.format(fig_dir, plot_name)
plot_names = np.append(plot_names, full_plot_name)
print('Writing plot PDF to {}'.format(full_plot_name))
plt.savefig(full_plot_name, bbox_inches='tight')
plt.close()



# print open command
command = 'open'
for pf in plot_names:
    command = '{} {}'.format(command, pf)
print()
print('Command to open plots generated by this script:')
print(command)
print()